<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!-- Template Design by TheWebhub.com | http://www.thewebhub.com | Released for free under a Creative Commons Attribution-Share Alike 3.0 Philippines -->
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <title>:: Parket Mont - Novi Sad, Srbija ::</title>
  <meta name="keywords" content="">
  <meta name="description" content="">
  <link href="style.css" rel="stylesheet" type="text/css" media="screen">
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
</head>
<body>
<div id="menu">
<ul>
  <li class="first"><a href="index.html">O nama</a></li>
  <li><a href="gipsani_radovi.html">Gipsani radovi</a></li>
  <li><a href="ponuda.html">Ponuda</a></li>
  <li><a href="prodavnica.html">Prodavnica</a></li>
  <li><a href="lajsne.html">Lajsne</a></li>
  <li><a href="galerija.php?id=1">Galerija</a></li>
  <li><a href="kalkulator.html">Kalkulator</a></li>
  <li><a href="kontakt.html">Kontakt</a></li>
</ul>
</div>
<div id="content">
<div id="content-wrapper-top">
<div id="content-wrapper-btm">
<div id="left"><br>
<span style="text-decoration: underline;"><img
 style="width: 251px; height: 332px;" alt="Parket Mont" src="images/logo02.jpg"></span><br>
</div>
<div id="right">
<div class="post"><span class="Apple-style-span"
 style="border-collapse: separate; color: rgb(0, 0, 0); font-family: 'Times New Roman'; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; font-size: medium;"><span
 class="Apple-style-span"
 style="font-family: sans-serif; text-align: justify;">
<div style="text-align: justify;">
<p><b>U cenu je uračunato:</b></p>
<ul>
	<li>Isporuka parketa i ostalog potrebnog materijala</li>
	<li>Ugradnja parketa</li>
	<li>Brušenje</li>
	<li>Lakiranje 3 puta</li>
	<li>Poliranje (mašinski)</li>
	<li>Ugradnja ugaone parket lajsne</li>
	<li>Klasičan parket dimenzija 200-350x42, 52x21</li>
</ul>
<br/>
<hr/>
      <form method="post" action="">
      <table>
      <tr>
      	<td width=150'>Parket hrast S:</td>
      	<td><input name="hrasts" value="0" size="5" type="text"> m<sup>2</sup></td>
      </tr>
      <tr>
      	<td>Parket hrast R:</td>
      	<td><input name="hrastr" value="0" size="5" type="text"> m<sup>2</sup></td>
      </tr>
      <tr>
      	<td>Parket hrast VS:</td>
      	<td><input name="hrastvs" value="0" size="5" type="text"> m<sup>2</sup></td>
      </tr>
      <tr>
      	<td></td>
      	<td style='text-align: center;'><input name="ok" value="Izračunaj" type="submit"></td>
      </tr>
      </table>
      </form>
<?php
	$hrasts = $_POST['hrasts'];
	$hrastr = $_POST['hrastr'];
	$hrastvs = $_POST['hrastvs'];
	
	if(isset($_POST['ok']))	{
		$x = $hrasts*33;
		$y = $hrastr*28;
		$z = $hrastvs*24;
		
		$ukupno = $x + $y + $z;
		
		echo '<hr/><table>';
		echo "<tr><td width=150>Parket Hrast S:</td><td style='text-align: right;'><i>" .$x ."</i> &#8364</td></tr>"; 
		echo "<tr><td>Parket Hrast R:</td><td style='text-align: right;'><i>" .$y ."</i> &#8364</td></tr>";
		echo "<tr><td>ParketHrast VS:</td><td style='text-align: right;'><i>" .$z ."</i> &#8364</td></tr>";
		echo '</table><hr/>';
		echo "<b>Ukupno: " .$ukupno ." &#8364</b>";
	}
	else {
		echo '';
	}
?>
</div>
</span></span><br class="Apple-interchange-newline">
</div>
</div>
<div style="clear: both;">&nbsp;</div>
</div>
</div>
</div>
<div id="footer">
  <p class="link1"><a href="index.html">Početna</a>&nbsp;&nbsp;»&nbsp;&nbsp;<a href="gipsani_radovi.html">Gipsani radovi</a>&nbsp;&nbsp;»&nbsp;&nbsp;<a href="ponuda.html">Ponuda</a>&nbsp;&nbsp;»&nbsp;&nbsp;<a href="prodavnica.html">Prodavnica</a>&nbsp;&nbsp;»&nbsp;&nbsp;<a href="lajsne.html">Lajsne</a>&nbsp;&nbsp;»&nbsp;&nbsp;<a href="galerija.html">Galerija</a>&nbsp;&nbsp;»&nbsp;&nbsp;<a href="kalkulator.html">Kalkulator</a>&nbsp;&nbsp;»&nbsp;&nbsp;<a href="kontakt.html">Kontakt</a></p>
<p class="copyright">Copyright © <a class="white" href="http://www.nebjak.net/" >NEBJAK.NET</a></p>
</div>
</body>
</html>
